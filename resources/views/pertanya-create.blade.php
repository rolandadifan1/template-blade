@extends('app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="container">
        <form method="POST" action="{{route('pertanyaan-store')}}">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Isi</label>
                <input type="text" class="form-control" name="isi" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">judul</label>
                <input type="text" class="form-control" name="judul" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <input type="hidden" class="form-control" name="profil_id" value="1" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <input type="hidden" class="form-control" name="jawaban_tepat_id" value="1" id="exampleInputPassword1">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </section>
    <!-- /.content -->
</div>
@endsection