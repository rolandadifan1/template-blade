<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');


// Route::get('/data-table', 'TableController@index')->name('table');
// Route::get('/pertanyaan', 'PertanyaanController@index')->name('pertanyaan');
// Route::get('/pertanyaan/create', 'PertanyaanController@create')->name('pertanyaan-create');
// Route::post('/pertanyaan/create', 'PertanyaanController@store')->name('pertanyaan-store');
// Route::get('/pertanyaan/{id}', 'PertanyaanController@show')->name('pertanyaan-show');
// Route::get('/pertanyaan/edit/{id}', 'PertanyaanController@edit')->name('pertanyaan-edit');
// Route::put('/pertanyaan/update/{id}', 'PertanyaanController@update')->name('pertanyaan-update');
// Route::delete('/pertanyaan/delete/{id}', 'PertanyaanController@destroy')->name('pertanyaan-destroy');

Route::resource('pertnyaan', 'PertanyaanController');
