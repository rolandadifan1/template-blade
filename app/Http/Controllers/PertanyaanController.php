<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $items = DB::table('pertanyaan')->get();
        return view('pertanyaan')->with([
            'items' => $items
        ]);
    }

    public function create()
    {

        return view('pertanya-create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        Pertanyaan::create($data);
        return redirect()->route('pertanyaan');
    }

    public function show($id)
    {
        $items = Pertanyaan::findOrFail($id);
        return view('pertanya-show')->with([
            'items' => $items
        ]);
    }

    public function edit($id)
    {
        $items = Pertanyaan::findOrFail($id);
        return view('pertanya-edit')->with([
            'items' => $items
        ]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $pertanyaan = Pertanyaan::findOrFail($id);
        $pertanyaan->save($data);
        return redirect()->route('pertanyaan');
    }

    public function destroy($id)
    {
        $items = Pertanyaan::findOrFail($id);
        $items->delete();
        return view('pertanyaan');
    }
}
