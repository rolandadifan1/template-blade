@include('hedder')
<div class="wrapper">

    <!-- Navbar -->
    @include('navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('sidebar')

    <!-- Content Wrapper. Contains page content -->
    @yield('content')
    <!-- /.content-wrapper -->


    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('script')